### Installation

You can install the released version of landclimhelpers from [gitlab](https://gitlab.irstea.fr) with:

``` r
library(devtools)
devtools::install_git('https://gitlab.irstea.fr/bjoern.reineking/LandclimHelpers.git')
```
